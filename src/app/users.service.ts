import { Injectable } from '@angular/core';
import usersJson from '../app/users.json'
import { User } from './interfeces';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor() { }
  getAll():User[]{
    return usersJson
  }
}
