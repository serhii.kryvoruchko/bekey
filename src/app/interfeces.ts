export interface User {
    _id?: string,
    address: string,
    age: number,
    company: string,
    email: string,
    name:string,
    registered?:string,
    about?:string,
    phone?:string,
    gender?:string,
    eyeColor?:string,
    picture?:string,
    guid?:string,
    balance?:string,
    latitude:number,
    longitude:number,
    tags?:any
}