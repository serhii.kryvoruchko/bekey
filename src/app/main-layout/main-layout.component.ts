import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { UsersService } from '../users.service';

import {MatTableDataSource} from '@angular/material/table';
import { User } from '../interfeces';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit  {


  displayedColumns: string[] = ['id','name', 'age', 'address', 'company','email'];
  dataSource :MatTableDataSource<User>;
  mainConfiguration: any;
  constructor(private userSevice: UsersService,private router: Router) {
    this.dataSource = new MatTableDataSource();
   
   }

  ngOnInit(): void {
    this.mainConfiguration = this.userSevice.getAll();
    localStorage.setItem('users',JSON.stringify(this.mainConfiguration))
    console.log('mainConfiguration',this.mainConfiguration)
    this.dataSource = this.mainConfiguration
    console.log('dataSource',this.dataSource)
    
  }


}
