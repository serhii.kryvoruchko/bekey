import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../interfeces';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {
  
  id:string = ''
  user!:User
  constructor(private route:ActivatedRoute,private userService: UsersService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id')!;
    this.user = this.userService.getAll().find(u=>u._id === this.id)!
    
  }
  
}
