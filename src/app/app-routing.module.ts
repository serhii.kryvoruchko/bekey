import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { UserPageComponent } from './user-page/user-page.component';

const routes: Routes = [
    {path:'', component: MainLayoutComponent},
    { path: 'users/:id',component: UserPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
